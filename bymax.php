<?
#----------------------------------------------------------------------------------------------------------
$name 		= $_GET['name'];
$sdc		= isset($_GET['sdc'])?$_GET['sdc']:"99168";
$service	= isset($_GET['service'])?$_GET['service']:"";
$phone		= isset($_GET['phone'])?$_GET['phone']:"";

#----------------------------------------------------------------------------------------------------------
include_once '../wurfl_config_standard.php';
include_once '../lib/function.php';
require_once("../lib/classes/Mysql5.php");
require_once("../lib/classes/SqlServer.php");


date_default_timezone_set ( 'Asia/Jakarta' );

#----------------------------------------------------------------------------------------------------------
$requestingDevice = $wurflManager->getDeviceForHttpRequest($_SERVER);
$is_wireless = ($requestingDevice->getCapability('is_wireless_device') == 'true');
$device = $wurflManager->getDeviceForUserAgent($_SERVER['HTTP_USER_AGENT']);

$brand 			= strtoupper($device->getCapability('brand_name'));
$model 			= strtoupper($device->getCapability('model_name'));
$device_os 		= strtoupper($device->getCapability('device_os'));
$mobile_browser = strtoupper($device->getCapability('mobile_browser'));

$ip=ip2long($_SERVER['REMOTE_ADDR']);
//Range_IP_TSEL
$ip1a=ip2long("39.192.0.0"); $ip1b=ip2long("39.255.255.255");
$ip2a=ip2long("182.0.0.0"); $ip2b=ip2long("182.15.255.255");
$ip3a=ip2long("114.120.0.0"); $ip3b=ip2long("114.127.255.255");
$ip4a=ip2long("221.132.192.0"); $ip4b=ip2long("221.132.255.255");
$ip5a=ip2long("202.3.208.0"); $ip5b=ip2long("202.3.223.255");
$ip6a=ip2long("107.167.0.0"); $ip6b=ip2long("107.167.255.255");


if(empty($phone)){
	$phone	= $brand;
	if (strpos($device_os,'ANDROID') !== false || strpos($device_os,'WINDOWS') !== false || strpos($device_os,'IPHONE') !== false){
		$phone = $device_os;
	}
	if($phone=='RIM'){
		$phone="BLACKBERRY";
	}
}

$referer = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"";
$address = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:"";

#----------------------------------------------------------------------------------------------------------
$mysql = new Mysql5();
if(!$mysql->connect()) exit("DB Failed");
#----------------------------------------------------------------------------------------------------------

$sql = "select ct.serv_code, c.content_code, u.url, c.content_price from  richcontent.t_content_url u, richcontent.t_content c, richcontent.t_category ct ";
$sql.= "where ct.cat_code=c.cat_code  ";
$sql.= "and u.content_code=c.content_code "; 
$sql.= "and c.content_name='$name'  ";
if(!empty($service)){
	//$sql.= "and ct.serv_code='$service'  ";
}
if($phone!=''){
	//$sql.= "and (hp_vendor='$phone')";
}
$sql.= "limit 1 ";

$rows = $mysql->select($sql);

//$mysql->disconnect();
#----------------------------------------------------------------------------------------------------------

if($rows){
	$code 		= $rows[0]['content_code'];
	$keyword 	= $service." ".$code." I";
	$price 		= $rows[0]['content_price'];  
	
	$isp = get_ip_isp($_SERVER['REMOTE_ADDR']);
	$date = date("YmdHis");
	#----------------------------------------------------------------------------------------------------------
	if(preg_match('/Excelcomindo|Excelcom|XL|AXIATA/i',$isp)){
		$bcomm_url_callback = "http://wap.rajakonten.com/xl_wap_callback.php?d=".urlencode($date)."&keyword=".urlencode($keyword)."&sdc=$sdc&";
		$xl_url_charging = "http://112.215.81.8/Wap_action.jsp?content=".urlencode($keyword)."&dest=$sdc&success=".urlencode($bcomm_url_callback);
		header("Location: $xl_url_charging");
		
		//$xl_konfirmasi="http://wap.rajakonten.com/xl_confirmation.php?layanan=".urlencode($service)."&url=".urlencode($xl_url_charging);
		//header("Location: $xl_konfirmasi");
	}
	#----------------------------------------------------------------------------------------------------------
	else if(preg_match('/Telkomsel|TSEL|Telekomunikasi Selular/i',$isp) || $ip1a <= $ip && $ip <= $ip1b || $ip2a <= $ip && $ip <= $ip2b || $ip3a <= $ip && $ip <= $ip3b || $ip4a <= $ip && $ip <= $ip4b || $ip5a <= $ip && $ip <= $ip5b || $ip6a <= $ip && $ip <= $ip6b){
		//header("Location: notsupportednetwork.php?isp=".urlencode($isp));
		/*
		$bcomm_url_callback = "http://wap.rajakonten.com/xl_wap_callback.php?d=".urlencode($date)."&keyword=".urlencode($keyword)."&sdc=$sdc&";
		$xl_url_charging = "http://112.215.81.8/Wap_action.jsp?content=".urlencode($keyword)."&dest=$sdc&success=".urlencode($bcomm_url_callback);
		header("Location: $xl_url_charging");
		*/
		//$bcomm_url_callback = "http://202.3.208.79:8000/generate.php?cp_name=BCOMM&pwd=bco122mm&sid=bcomm_games&programid=GG";
	
		$token = get_token(substr($service,0,2));
		$tsel_url_confirm = "http://auth.telkomsel.com/transaksi/konfirmasi?token=".$token;
		
		if(strlen($token) < 5) $token = uniqid();
		
		$insertToken = "INSERT INTO request_acceptor.games_token (token, game_code) VALUES ('".$token."','".$code."')";
		$mysql->insert($insertToken);
		
		header("Location: $tsel_url_confirm");
	}
	#----------------------------------------------------------------------------------------------------------
	else if(preg_match('/Indosat|ISAT|GGSN/i',$isp)){
		
		$sdc="99168"; $price="5000";
		$service="GAME1"; $keyword="GAME1";
		
		//$service = "TES";
		//$keyword = "TES ".$code;
		//$price = 3000;
		
		//$bcomm_url_callback = "http://202.43.173.196/isat_wap.php";
		$bcomm_url_callback = "http://wap.rajakonten.com/isat_wap_callback.php?keyword=".urlencode($keyword)."&";
		$isat_url = "http://202.152.162.192:8002/GetId?eid=ISATLP2&cpName=BCOMM&serviceName=$service&sc=$sdc&keyword=".urlencode($keyword)."&tariff=$price&cs=0217256688&url=".urlencode($bcomm_url_callback);
		error_log($isat_url);
		header("Location: $isat_url");
	}
	#----------------------------------------------------------------------------------------------------------
	else{
		if($service=="GF")
		{
			$bcomm_url_callback = "http://wap.rajakonten.com/tsel_wap_callback.php?d=".urlencode($date)."&keyword=".urlencode($keyword)."&sdc=$sdc&";
			header("Location: $bcomm_url_callback");
		}
		else
		{
			//header("Location: notsupportednetwork.php?isp=".urlencode($isp));
		/*
		$bcomm_url_callback = "http://wap.rajakonten.com/xl_wap_callback.php?d=".urlencode($date)."&keyword=".urlencode($keyword)."&sdc=$sdc&";
		$xl_url_charging = "http://112.215.81.8/Wap_action.jsp?content=".urlencode($keyword)."&dest=$sdc&success=".urlencode($bcomm_url_callback);
		header("Location: $xl_url_charging");
		*/
		//$bcomm_url_callback = "http://202.3.208.79:8000/generate.php?cp_name=BCOMM&pwd=bco122mm&sid=bcomm_games&programid=GG";
	
		$token = get_token(substr($service,0,2));
		$tsel_url_confirm = "http://auth.telkomsel.com/transaksi/konfirmasi?token=".$token;
		
		if(strlen($token) < 5) $token = uniqid();
		
		$insertToken = "INSERT INTO request_acceptor.games_token (token, game_code) VALUES ('".$token."','".$code."')";
		$mysql->insert($insertToken);
		
		header("Location: $tsel_url_confirm");
		}
	}
	
}
else{
	header("Location: browse.php?name=$name&sdc=$sdc&service=$service");
}

$mysql->disconnect();

?>

